.ONESHELL:
SHELL=/bin/bash
.SHELLFLAGS=-ec

.PHONY: all build check check-links

WGET = wget -q -O- --no-check-certificate --spider --user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0" --server-response
WGET_BACKOFF = 1
FAILURE_THRESHOLD = 3

all: check-links build

check: check-links

check-links:
	FAILURES="$$(
	  find -type f -name 'content.html' -exec sed -n -r 's/href="(ht|f)tp/\n&/gp' -- {} + \
	  | sed -n -r '/href="(ht|f)tp/s/.*href="([^"]*)".*/\1/p' \
	  | sort -u \
	  | parallel -j 1 -- '
	    $(WGET) -- {} 2>&1 \
	    | grep -E -q "HTTP/[0-9.]* ([^4]|4[^0]|40[^4])" \
	    || echo {}
	    sleep $(WGET_BACKOFF)
	  '
	)"
	NUM_FAILURES=$$(wc -l <<< "$$FAILURES")
	if (( $$NUM_FAILURES > $(FAILURE_THRESHOLD) ))
	then
	  printf 'Našel jsem rozbité odkazy (%d) na webu fi.muni.cz/lemma/PB029:\n\n' $$NUM_FAILURES 1>&2
	  parallel -j 1 -I '[]' -- '
	    printf -- "- %s (nachází se v %s)\n" [] "$$(
	      echo $$(
	        find -type f -name 'content.html' -exec grep -l -F [] -- {} +
	      )
	    )"
	  ' <<< "$$FAILURES" 1>&2
	  printf '\n' 1>&2
	  exit 1
	fi

build:
	make $(patsubst %/content.html, %/index.html, $(wildcard public/practices/*/content.html)) public/index.html public/finalwork/index.html
	make -C public/miniwork

%/index.html: %/content.html public/template.html
	./template.sh $^ > $@.new
	mv $@.new $@
