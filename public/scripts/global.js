function getDateInfo(date, text) {
	function getFull(value) {
	  if (value < 10) {
	    return "0" + value;
	  } else {
	    return value;
	  }
	}

	function getTimezoneString(date) {
	  var value = (date.getTimezoneOffset() / 60);

	  if (value > 0) {
	    return "-" + getFull(value) + ":00";
	  } else {
	    if (value == 0) {
	      return "Z";
	    } else {
	      return "+" + getFull((-1) * value) + ":00";
	    }
	  }
	}

	document.write(text + " ");
	document.write(date.getFullYear() + "-" +
		getFull((date.getMonth()) + 1) + "-" +
		getFull(date.getDate()) + "T" +
		getFull(date.getHours()) + ":" +
		getFull(date.getMinutes()) + ":" +
		getFull(date.getSeconds()) +
		getTimezoneString(date));
}

function getLastModified() {
	getDateInfo(new Date(document.lastModified), "Posledn\u00ED zm\u011Bna:");
}

function getCreateTime() {
	getDateInfo(new Date(), "Vygenerov\u00E1no:");
}
