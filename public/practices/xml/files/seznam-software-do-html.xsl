<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output 
        method="html" 
        encoding="UTF-8" 
        indent="yes" 
        doctype-public="-//W3C//DTD HTML 4.01//EN" 
        doctype-system="http://www.w3.org/TR/html4/strict.dtd"/>

    <xsl:template match="/programs">
        <html>
            <head>
                <meta http-equiv="Content-Language" content="cs"/>
                <meta http-equiv="Cache-Control" content="no-cache"/>
                <meta http-equiv="Pragma" content="no-cache"/>
                <meta name="description" content="Seznam instalačních souborů programů s jejich popisem a adresou domovské stránky."/>
                <meta name="keywords" content="seznam,programy,instalační soubory,homepage,domovské stránky,odkazy"/>
                <style type="text/css">
                    <![CDATA[
                        body {
                            min-width: 39em;
                            margin: auto;
                            padding: 0em 2em;
                            background-color: #FFCC7F;
                            font-family: sans-serif, serif;
                            color: #000000;
                        }
                        #zahlavi {
                            position: relative;
                            top: 0em;
                            min-width: 35em;
                            max-width: 50em;
                            height: 5em;
                            margin: 0em auto;
                            padding: 1em;
                            background-color: #000000;
                            color: #FFFFFF;
                        }
                        #obsah {
                            position: relative;
                            min-width: 35em;
                            max-width: 52em;
                            margin: 0em auto;
                            padding: 1em 0em;
                            background-color: #FFF3E0;
                            color: #000000;
                        }
                        #text {
                            padding: 0em 1em;
                        }
                        a {
                            color: #807340;
                        }
                        a:hover {
                            color: #FF6600;
                        }
                        h3 {
                            margin: 0.5em 0em 0em 0em;
                        }
                        .program-background h3 {
                            background-color: #FFF3E0;
                            color: #000000;
                        }
                        .program-group-background h3 {
                            background-color: #807340;
                            color: #000000;
                        }
                        .program-name {
                            padding: 0em 0.5em;
                            background-color: #E6E1CF;
                            color: #000000;
                        }
                        .program-group-name {
                            padding: 0em 0.5em;
                            background-color: #807340;
                            color: #E6E1CF;
                        }
                        .program-background {
                            margin: 0;
                            padding: 0.5em 0.3em 0.3em 2em;
                            list-style: none;
                            background-color: #E6E1CF;
                            color: #000000;
                        }
                        .program-group-background {
                            padding: 0.5em 0em 0em 2em;
                            background-color: #807340;
                            color: #000000;
                        }
                        div.value {
                            text-indent: 1.5em;
                        }
                        .label {
                            font-style: italic;
                        }
                    ]]>
                </style>
                <title><xsl:value-of select="@title"/></title>
            </head>
            <body>
                <div id="zahlavi">
                    <h1><xsl:value-of select="@title"/></h1>
                </div>
                <div id="obsah">
                    <div id="text">
                        <xsl:apply-templates select="*"/>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="program-group">
        <h3><span class="program-group-name"><xsl:value-of select="@name"/></span></h3>
        <div class="program-group-background">
            <xsl:apply-templates select="*"/>
        </div>
    </xsl:template>

    <xsl:template match="program">
        <xsl:apply-templates select="name"/>
        <ul class="program-background">
            <li><xsl:apply-templates select="filename"/></li>
            <li><xsl:apply-templates select="version"/></li>
            <li><xsl:apply-templates select="homepage"/></li>
            <li><xsl:apply-templates select="description"/></li>
        </ul>
    </xsl:template>

    <xsl:template match="name">
        <h3><span class="program-name"><xsl:value-of select="text()"/></span></h3>
    </xsl:template>

    <xsl:template match="filename">
        <span class="label">Instalační soubor:</span>
        <xsl:text> </xsl:text>
        <a class="value">
            <xsl:attribute name="href">
                <xsl:for-each select="ancestor::program-group"><xsl:value-of select="@name"/>/</xsl:for-each>
                <xsl:value-of select="../name"/>/<xsl:value-of select="../filename"/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="boolean(../filename/text())">
                    <xsl:value-of select="../filename"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="../name"/>
                </xsl:otherwise>
            </xsl:choose>
        </a>
        <br/>
    </xsl:template>

    <xsl:template match="version">
        <span class="label">Verze:</span>
        <xsl:text> </xsl:text>
        <span class="value"><xsl:value-of select="text()"/></span>
        <br/>
    </xsl:template>

    <xsl:template match="homepage">
        <span class="label">Homepage:</span>
        <xsl:text> </xsl:text>
        <a class="value">
            <xsl:attribute name="href"><xsl:value-of select="text()"/></xsl:attribute>
            <xsl:value-of select="text()"/>
        </a>
        <br/>
    </xsl:template>

    <xsl:template match="description">
        <span class="label">Popis:</span>
        <div class="value"><xsl:value-of select="text()"/></div>
    </xsl:template>

</xsl:transform>
